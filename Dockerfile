FROM python:3.7-slim
ARG INLINE_SCAN_HASH=bcf59d79defde7f3d7352188fc979ed4da6e1ff98f822392c58ce189d9d1319c
ARG INLINE_SCAN_VERSION=v0.10.2

COPY requirements.txt /

RUN apt-get -y update && \
    apt-get -y install curl && \
    pip install -r /requirements.txt && \
    apt-get -y clean

RUN curl -fSL -o /inline_scan "https://ci-tools.anchore.io/inline_scan-${INLINE_SCAN_VERSION}" && \
    echo "$INLINE_SCAN_HASH *inline_scan" | sha256sum -c - && \
    chmod +x /inline_scan
    
COPY data /
COPY pipe /
COPY pipe.yml /

ENTRYPOINT ["python3", "/pipe.py"]
