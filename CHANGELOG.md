# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.17

- patch: Bump version of inline can to v0.10.2

## 0.2.16

- patch: Bump version of inline can to v0.10.0

## 0.2.15

- patch: Update version of inline scan to 0.9.4

## 0.2.14

- patch: Bump inline scan to 0.9.3

## 0.2.13

- patch: Bump version of inline scan to 0.9.2

## 0.2.12

- patch: Bump inline scan version to 0.2.12

## 0.2.11

- patch: Bump version of inline scan to v0.9.0

## 0.2.10

- patch: Bump version of inline scan to 0.8.2

## 0.2.9

- patch: Bump inline scan to v0.8.1

## 0.2.8

- patch: Bump inline scan to v0.8.0

## 0.2.7

- patch: Add category key, remove icon key from pipe.yml

## 0.2.6

- patch: Update pipe.yml to match new BitBucket guidelines

## 0.2.5

- patch: Include update to pipe.yml in updating to inline scan v0.7.3

## 0.2.4

- patch: Bump version of inline scan to 0.7.3

## 0.2.3

- patch: update pipe.yml to accurately reflect latest pipe version

## 0.2.2

- patch: New release adding ability to override image analysis timeout default using the ANALYSIS_TIMEOUT_SECONDS input parameter

## 0.2.1

- patch: Bump version of inline scan to v0.7.2

## 0.2.0

- minor: Update anchore scanner version to v0.7.1

## 0.1.2

- patch: add code-insights tag and pipe version update for release 0.1.2

## 0.1.1

- patch: README example, requirements.txt version locks, and pipe reference version updates

## 0.1.0

- minor: Initial pipe release version 0.1.0
